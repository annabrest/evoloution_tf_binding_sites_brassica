---
title: "R Notebook"
output: html_notebook
---
Aim of this script:
The puropuse of this script is to find the N of promoters and overlap betweenn promoters in Ara and Brapa for each TF

## 1. Load the required packages
```{r message=FALSE, include=FALSE}
pkgs <- c('here', 'tidyverse','RColorBrewer','gplots',
          'randomForest', 'broom', 'gridExtra',
          'rtracklayer', 'Biostrings', 'GenomicRanges', 'ggpubr')

lapply(pkgs, library, character.only = TRUE)
rm(pkgs)

# source(file = here("scripts", "My_functions.R"))


set.seed(999)
#library(verification)
#roc.plot(obs.pred[,"Observed"], obs.pred[,"Prob1"])
```


## 2. Load Data
```{r}

# Results of the TF bs predictions from both starndes
Brapa3_TF_results_scan_rc <- 
  read_csv(here("raw_data", "TF_rc_scan_results_Brapa3_promoters1500_TF_family.csv"))
Ara_TF_results_scan_rc <- 
  read_csv(here("raw_data", "TF_rc_scan_results_promoters1500_TF_family.csv"))

Ara_TF_results_orthologs_rc <- read_csv("output_data/Ara_TF_results_orthologs_rc.csv")
Brapa3_TF_results_orthologs_rc <- read_csv("output_data/Brapa3_TF_results_orthologs_rc.csv")

Brapa3_TF_results_3genomes_rc <- read_csv("output_data/Brapa3_TF_results_3genomes_rc.csv") # not full table, use Brapa3_TF_results_rc.csv for full data.frame

# Brapa and Ara orthologs; Load from "/output_data"; created in 01_preprocess_data.Rmd
# Change this file later to more updated file
Brapa3_genomes_updated <- 
  read_csv(here("output_data/Brapa3_genomes_updated.csv"))

# top TF predicted by random forest
top_TF_rf <- c("ABI3VP1_tnt.REM16_col_a_m1", "ARF_tnt.ARF16_col_b_m1", "E2FDP_tnt.DEL2_col_a_m1",	"E2FDP_tnt.DEL1_colamp_a_m1", "E2FDP_tnt.DEL2_colamp_a_m1", "E2FDP_tnt.DEL1_col_a_m1")

# list of TFs
TF_list <- sort(unique(Brapa3_TF_results_scan_rc$motif))
length(TF_list)

```


## write function to calculate N of promoters predicted in Arabidopsis and Brassica for each TF, and then to check for overlaps
## the output is data.frame containg this data for each TF
```{r}

count_N_prom <- function(my_motif = my_motif){

# for TF count N of promoters in Ara with at least 1 bs
N_promoters_Ara <- 
Ara_TF_results_scan_rc %>% 
  filter(motif == my_motif) %>% select(sequence) %>% unique() %>% count()

# for TF count N of promoters of orthologs in Ara with at least 1 bs
N_promoters_Ara_orth <- 
Ara_TF_results_orthologs_rc %>% 
  filter(motif == my_motif) %>% select(sequence) %>% unique() %>% count()

# for TF count N of promoters in Brapa with at least 1 bs 
N_promoters_Brapa <- 
Brapa3_TF_results_scan_rc %>% 
  filter(motif == my_motif) %>% select(sequence) %>% unique() %>% count()

# for TF count N of promoters of orthologs in Brapa with at least 1 bs 
N_promoters_Brapa_orth <- 
Brapa3_TF_results_orthologs_rc %>% 
  filter(motif == my_motif) %>% select(sequence) %>% unique() %>% count()

# Extract list of promoters bound by TF with at least 1 bs in Ara orthologs
promoters_Ara_orth_motif <- 
  Ara_TF_results_orthologs_rc %>% 
  filter(motif == my_motif) %>% select(sequence) %>% unique() 

# Extract list of promoters bound by TF with at least 1 bs in Brapa orthologs
promoters_Brapa_orth_motif <- 
Brapa3_TF_results_orthologs_rc %>% 
  filter(motif == my_motif) %>% select(AGI) %>% unique() 

# find N overlaping promoters in Ara and Brapa orthologs bound by the same TFs
Overlap_Ara_Brapa_motif <- promoters_Ara_orth_motif %>% 
  inner_join(.,promoters_Brapa_orth_motif, by = c("sequence" = "AGI"))

N_promoters_data <- setNames(data.frame(
  as.data.frame(my_motif), N_promoters_Ara, N_promoters_Ara_orth, N_promoters_Brapa, N_promoters_Brapa_orth, nrow(Overlap_Ara_Brapa_motif)),
  c("motif", "N_promoters_Ara", "N_promoters_Ara_orth", "N_promoters_Brapa", "N_promoters_Brapa_orth", "Overlap_Ara_Brapa_orth"))

return(N_promoters_data)  

}
  
# Calculate N of promoters and overlaps in predicted promoters in Arabidopsis and Brassica

N_promoters_TFs <- lapply(TF_list, function(x)count_N_prom(my_motif = x))

N_promoters_TFs_df <- bind_rows(N_promoters_TFs)

N_promoters_TFs_df %>% 
  filter(Overlap_Ara_Brapa_orth > 0) %>% 
  mutate(pct_overlap_Ara = Overlap_Ara_Brapa_orth/N_promoters_Ara_orth, pct_overlap_Brapa = Overlap_Ara_Brapa_orth/N_promoters_Brapa_orth) %>% 
  arrange( desc(pct_overlap_Brapa), desc(pct_overlap_Ara))


N_promoters_TFs_df  


```

##--Save output
```{r}
# write_csv(N_promoters_TFs_df, here("output_data/Ara_Brapa_overlap/N_promoters_TFs_Ara_Brapa_overlap.csv"))
```

###-- Normalise by tot_N_promoters in each data set
```{r}
# Calculate tot_N_promoters
tot_N_promoters <- setNames(data.frame( Ara_TF_results_scan_rc %>% select(sequence) %>% distinct() %>% count(),
                              Brapa3_TF_results_scan_rc %>% select(sequence) %>% distinct() %>% count(),
                              Ara_TF_results_orthologs_rc %>% select(sequence) %>% distinct() %>% count(),
                              Brapa3_TF_results_orthologs_rc %>% select(sequence) %>% distinct() %>% count() ),
                              c("Ara", "Brapa" ,  "Ara_orth" , "Brapa_orth"))


N_promoters_TFs_df <- N_promoters_TFs_df %>% 
  mutate(N_promoters_Ara_norm = (N_promoters_Ara/tot_N_promoters$Ara),
         N_promoters_Ara_orth_norm = (N_promoters_Ara_orth/tot_N_promoters$Ara_orth),
         N_promoters_Brapa_norm = (N_promoters_Brapa/tot_N_promoters$Brapa),
         N_promoters_Brapa_orth_norm = (N_promoters_Brapa_orth/tot_N_promoters$Brapa_orth)
         )
  
  
  N_promoters_TFs_df %>% arrange(desc(N_promoters_Ara_orth))
```
```{r}
tot_N_promoters
```


```{r}
# N_promoters_TFs_df %>% 
#   mutate(pct_overlap_Ara = 100*Overlap_Ara_Brapa_orth/N_promoters_Ara_orth, pct_overlap_Brapa = 100*Overlap_Ara_Brapa_orth/N_promoters_Brapa_orth) %>% 
#   ggplot(aes(pct_overlap_Brapa, pct_overlap_Ara)) +
#   geom_point() +
#   xlim(c(0, 100))
#   

```

```{r}
N_promoters_TFs_df %>% 
  ggplot(aes(N_promoters_Brapa_orth_norm, N_promoters_Ara_orth_norm)) +
  geom_point() 

N_promoters_TFs_df %>% 
  ggplot(aes(N_promoters_Brapa_norm, N_promoters_Ara_norm)) +
  geom_point() 

```




###--Plot Overlap between Promoters which bound to the same TFs 
```{r}
# 
# N_promoters_TFs_df %>% 
#   mutate(overlap_Ara_norm = Overlap_Ara_Brapa_orth/tot_N_promoters$Ara_orth,
#          overlap_Brapa_norm = Overlap_Ara_Brapa_orth/tot_N_promoters$Brapa_orth) %>% 
#   ggplot(aes(N_promoters_Brapa_orth_norm, overlap_Brapa_norm)) +
#   geom_point()  
# 
# N_promoters_TFs_df %>% 
#   mutate(overlap_Ara_norm = Overlap_Ara_Brapa_orth/tot_N_promoters$Ara_orth,
#          overlap_Brapa_norm = Overlap_Ara_Brapa_orth/tot_N_promoters$Brapa_orth) %>% 
#   ggplot(aes(N_promoters_Ara_orth_norm, overlap_Ara_norm)) +
#   geom_point()  


```

###--Plot Overlaps between Promoters which bound to the same TFs normalised by N_total_promoters bound by each TF
```{r}
###--Plot Overlaps normalised by total bound by each TF
N_promoters_TFs_df %>% 
  mutate(overlap_Ara_norm_TF = Overlap_Ara_Brapa_orth/N_promoters_Ara_orth_norm,
         overlap_Brapa_norm_TF = Overlap_Ara_Brapa_orth/N_promoters_Brapa_orth_norm) %>% 
  ggplot(aes(overlap_Brapa_norm_TF, overlap_Ara_norm_TF)) +
  geom_point() +
  geom_smooth(method=lm, fullrange = TRUE, size=0.1) + # Remove confidence intervals
  labs(title = "N of promoters bound by each TF in both species,
       normalised by tot_N_promoters bound by this TF each specie")

N_promoters_TFs_df %>% 
  mutate(overlap_Ara_norm = Overlap_Ara_Brapa_orth/N_promoters_Ara_orth,
         overlap_Brapa_norm = Overlap_Ara_Brapa_orth/N_promoters_Brapa_orth) %>% 
  ggplot(aes(overlap_Brapa_norm, overlap_Ara_norm)) +
  geom_point() +
  geom_smooth(method=lm, fullrange = TRUE, size=0.1) + # Remove confidence intervals
  labs(title = "N of promoters bound by each TF in both species,
       normalised by tot_N_promoters annotated in each specie")




N_promoters_TFs_df %>% 
  mutate(overlap_Ara_norm_TF = Overlap_Ara_Brapa_orth/N_promoters_Ara_orth_norm,
         overlap_Brapa_norm_TF = Overlap_Ara_Brapa_orth/N_promoters_Brapa_orth_norm) %>% 
  ggplot(aes(N_promoters_Ara_orth_norm, overlap_Ara_norm_TF)) +
  geom_point() +
  geom_smooth(method=lm, fullrange = TRUE, size=0.1) + # Remove confidence intervals
  labs(title = "N of promoters bound by each TF in Arabidopsis,
       normalised by tot_N_promoters annotated in each specie")


N_promoters_TFs_df %>% 
  mutate(overlap_Ara_norm_TF = Overlap_Ara_Brapa_orth/N_promoters_Ara_orth_norm,
         overlap_Brapa_norm_TF = Overlap_Ara_Brapa_orth/N_promoters_Brapa_orth_norm) %>% 
  ggplot(aes(N_promoters_Brapa_orth_norm, overlap_Brapa_norm_TF)) +
  geom_point() +
  geom_smooth(method=lm, fullrange = TRUE, size=0.1) # Remove confidence intervals
   

```


```{r}
N_promoters_TFs_df %>% 
  mutate(overlap_Ara_norm_TF = Overlap_Ara_Brapa_orth/N_promoters_Ara_orth_norm,
         overlap_Brapa_norm_TF = Overlap_Ara_Brapa_orth/N_promoters_Brapa_orth_norm,
         Ara_to_Brapa_ratio = overlap_Ara_norm_TF/overlap_Brapa_norm_TF) %>% #arrange(desc(Ara_to_Brapa_ratio))
  filter(Ara_to_Brapa_ratio > 1.5 | Ara_to_Brapa_ratio < 0.75) %>% arrange(desc(overlap_Ara_norm_TF))
   

```








```{r}
N_promoters_TFs_df %>% 
  mutate(overlap_Ara_norm = Overlap_Ara_Brapa_orth/N_promoters_Ara_orth_norm, overlap_Brapa_norm = Overlap_Ara_Brapa_orth/N_promoters_Brapa_orth_norm) %>% 
  ggplot(aes(N_promoters_Brapa_orth, Overlap_Ara_Brapa_orth)) +
  geom_point()  

N_promoters_TFs_df %>% 
  mutate(overlap_Ara_norm = Overlap_Ara_Brapa_orth/N_promoters_Ara_orth_norm, overlap_Brapa_norm = Overlap_Ara_Brapa_orth/N_promoters_Brapa_orth_norm) %>% 
  ggplot(aes(N_promoters_Ara_orth, Overlap_Ara_Brapa_orth)) +
  geom_point()  


```





```{r}

```








```{r}
top_TF_rf
```

```{r}
top_TF_rf

my_motif <- "E2FDP_tnt.DEL1_col_a_m1"
TF_overlap <- 
  function(my_motif = my_motif){
    
 promoters_Ara_orth_motif <- 
  Ara_TF_results_orthologs_rc %>% 
  filter(motif == my_motif) %>% select(sequence) %>% unique() 

promoters_Brapa_orth_motif <- 
Brapa3_TF_results_orthologs_rc %>% 
  filter(motif == my_motif) %>% select(AGI) %>% unique() 


Overlap_Ara_Brapa_motif <- promoters_Ara_orth_motif %>% 
  inner_join(.,promoters_Brapa_orth_motif, by = c("sequence" = "AGI"))


return(Overlap_Ara_Brapa_motif)

}


lapply(my_motif, function(x)TF_overlap(my_motif = x))




my_motif <- "ABI3VP1_tnt.REM16_col_a_m1"

promoters_Ara_orth_motif <- 
  Ara_TF_results_orthologs_rc %>% 
  filter(motif == my_motif) %>% select(sequence) %>% unique() 

promoters_Brapa_orth_motif <- 
  Brapa3_TF_results_orthologs_rc %>% 
  filter(motif == my_motif) %>% select(AGI) %>% unique() 


Overlap_Ara_Brapa_motif <- promoters_Ara_orth_motif %>% 
  inner_join(.,promoters_Brapa_orth_motif, by = c("sequence" = "AGI"))


Overlap_Ara_Brapa_motif
dim(Overlap_Ara_Brapa_motif)
nrow(Overlap_Ara_Brapa_motif)


```



```{r}



if (length(Overlap_Ara_Brapa_motif) !=0) {
  write_csv(Overlap_Ara_Brapa_motif, paste0("output_data/Ara_Brapa_overlap/"  ,my_motif, "overlap.csv"))
}

Ara_TF_results_orthologs_rc %>% 
  filter(motif == my_motif) %>%
inner_join(.,Overlap_Ara_Brapa_motif)


Brapa3_TF_results_orthologs_rc %>% 
  filter(motif == my_motif) %>%
inner_join(.,Overlap_Ara_Brapa_motif, by = c("AGI"="sequence"))
```








##-------------##################---
```{r}
list_data <- c("Ara_TF_results_scan_rc")
, "Ara_TF_results_orthologs_rc", "Brapa3_TF_results_scan_rc", "Brapa3_TF_results_3genomes_rc")

test <- lapply(list_data, function(x)(as.data.frame(mget("Ara_TF_results_scan_rc")) %>% filter(Ara_TF_results_scan_rc.motif == "ARF_tnt.ARF16_col_b_m1") %>% group_by(contains("sequence")) %>% 
  summarise(n_bs = n()) %>% ungroup() %>% summarise(mean(n_bs))
  ))

```


```{r}
# Ara_orthologs <- 
#   Brapa3_genomes_updated %>% 
#   filter(AGI != "-") %>%
#   select(AGI) %>% unique()
#   
# Brapa_orthologs <- 
#   Brapa3_genomes_updated %>% 
#   filter(AGI != "-") 
# Brapa_orthologs 
# 
# 
# Brapa3_genomes_updated %>% 
#   filter(AGI != "-") %>% 
#   inner_join(., Ara_orthologs, by=c("AGI" = "AGI"))
```

```{r}
Ara_TF_results_orthologs_rc %>% 
  filter(motif %in% top_TF_rf) %>% select(sequence) %>% unique()
  group_by(motif, sequence) %>% count() 


Ara_TF_results_orthologs_rc %>% 
  filter(motif %in% top_TF_rf) %>% 
  inner_join(.,Ara_orthologs, by = c("sequence" = "AGI")) %>% select(sequence) %>% unique()
  group_by(motif, sequence) %>% count() 

Brapa3_TF_results_3genomes_rc %>% 
  filter(motif %in% top_TF_rf) %>% 
  select(motif, sequence, AGI, genome) %>% 
  filter(AGI == "-") %>% 
  group_by(motif) %>% count()

Brapa3_TF_results_3genomes_rc %>% 
  filter(motif %in% top_TF_rf) %>% 
  select(motif, sequence, AGI, genome) %>% 
  filter(AGI != "-") %>% 
  group_by(motif) %>% count()


Brapa3_TF_results_scan_rc %>% 
  filter(motif %in% top_TF_rf) %>% 
  select(motif, sequence) %>% 
  group_by(motif) %>% count()

Brapa3_TF_results_3genomes_rc %>% 
  filter(motif %in% top_TF_rf) %>% 
  select(motif, sequence, AGI, genome) %>% 
  filter(AGI != "-") %>% select(AGI) %>% unique()
```