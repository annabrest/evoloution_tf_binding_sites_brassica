---
title:  "Analysis and Visualisation of TF binding sites in B.rapa vs Arabidopsis promoters"
output: html_notebook
---

# This script is similar to 02_plot_rata.Rmd, but based on rc_predictions (on both strands)
# This script reads in preprocessed data from the TF predictions in Brassica and Arabidopsis files created by 01_preprocessee_data.Rmd and runs visualisation and basic statistics on the data.

## 1. Load the required packages
```{r message=FALSE, include=FALSE}
pkgs <- c('here', 'tidyverse','RColorBrewer','gplots',
          'randomForest', 'broom', 'gridExtra',
          'rtracklayer', 'Biostrings', 'GenomicRanges', 'ggpubr')

lapply(pkgs, library, character.only = TRUE)
rm(pkgs)

# source(file = here("scripts", "My_functions.R"))

set.seed(100)
```

## 2. Load the data from output_data/
```{r message=FALSE}
file_pattern <-  "TF_results_rc|orthologs_rc"

files_to_load <- list.files(here("output_data"), pattern = file_pattern, full.names = F)
temp <- sapply(files_to_load, function(x) read_csv(here("output_data", x)))
names(temp) <- str_remove_all(names(temp), ".csv$")

list2env(temp ,.GlobalEnv)
rm(files_to_load, temp)


# Brapa3_TF_results_orthologs <- read_csv(here("output_data", "Brapa3_TF_results_orthologs.csv"))
# Ara_TF_results_orthologs <- read_csv(here("output_data/Ara_TF_results_orthologs.csv"))
rm(list= str_subset(ls(), "orthologs", negate = TRUE))

Brapa3_genomes_updated <- 
  read_csv(here("output_data/Brapa3_genomes_updated.csv"))

```

## 3. -------*Orthologs*------#######
## 3.1. Plot "Mean number of TF binding sites among promoters they bound" in Brapa vs Arabidopsis
### Only promoters having TF included in calculation
####-- Preprocess data
```{r}
Brapa_data <- Brapa3_TF_results_orthologs_rc
Arab_data <- Ara_TF_results_orthologs_rc

Brapa_data_plot <- Brapa_data %>% 
  filter(motif !="0") %>% 
  group_by(motif, sequence) %>% 
  summarise("N_bs_TF_promoter" = n()) %>%  # n of promoters with each motif
  group_by(motif) %>% 
  summarise("mean_bs_TF_promoter" = mean(N_bs_TF_promoter)) 

Arab_data_plot <- Arab_data %>% 
  filter(motif !="0") %>%
  group_by(motif, sequence) %>% 
  summarise("N_bs_TF_promoter" = n()) %>% 
  group_by(motif) %>% 
  summarise("mean_bs_TF_promoter" = mean(N_bs_TF_promoter)) 


Brapa_data_plot
Arab_data_plot

combined_data <- full_join(Brapa_data_plot, Arab_data_plot, by = "motif", suffix = c("_Brapa", "_Ara"))

```

####-- Basic plot
```{r}
plot_data <- combined_data

p1 <- ggplot(plot_data, mapping = aes(mean_bs_TF_promoter_Brapa, mean_bs_TF_promoter_Ara)) +
  geom_point(alpha = 0.4, size = 0.6) +
  xlim(0,max(plot_data$mean_bs_TF_promoter_Brapa, plot_data$mean_bs_TF_promoter_Ara)) +
  ylim(0,max(plot_data$mean_bs_TF_promoter_Brapa, plot_data$mean_bs_TF_promoter_Ara)) +
  labs(title = "Mean number of TF binding sites per promoter",
       y = "Arabidopsis",
       x = "Brapa") +
  theme(plot.title = element_text(size=12))


# add regression line
p2 <- ggplot(plot_data, mapping = aes(mean_bs_TF_promoter_Brapa, mean_bs_TF_promoter_Ara)) +
  geom_point(alpha = 0.4, size = 0.6) +
  xlim(0,max(plot_data$mean_bs_TF_promoter_Brapa, plot_data$mean_bs_TF_promoter_Ara)) +
  ylim(0,max(plot_data$mean_bs_TF_promoter_Brapa, plot_data$mean_bs_TF_promoter_Ara)) +
  labs(title = "Mean number of TF binding sites per promoter",
       y = "Arabidopsis",
       x = "Brapa")  +
  theme(plot.title = element_text(size=12)) +
  geom_smooth(method=lm, se=FALSE, fullrange = TRUE, size=0.1) # Remove confidence intervals
                                    # Extend the regression lines
p1
p2

```


####-- Nicer plot
```{r}
p3 <- ggpubr::ggscatter(plot_data, x = "mean_bs_TF_promoter_Brapa", y = "mean_bs_TF_promoter_Ara", 
          add = "reg.line", conf.int = TRUE, 
          cor.coef = TRUE, cor.method = "pearson",
          xlab = "Brapa", ylab = "Arabidopsis", title = "Mean number of TF binding sites per promoter")
p3

```

####-- add statistics
```{r}
# To calculate correlation:
model <- lm(mean_bs_TF_promoter_Ara ~ mean_bs_TF_promoter_Brapa, data = plot_data)

summary(model)
coef(model)

# Pearson correlation test:
res <- cor.test(plot_data$mean_bs_TF_promoter_Brapa, plot_data$mean_bs_TF_promoter_Ara, 
                    method = "pearson")
res
# Extract the p.value
res$p.value

# Extract the correlation coefficient
res$estimate
#       cor 
# 0.9345368 

```



## Plot TFs with "Mean number of TF binding sites per promoter" which are not correlated in Brapa and Arabidopsis
```{r}
# test <- combined_data %>% 
#   mutate(correlation = mean_bs_TF_promoter_Brapa/mean_bs_TF_promoter_Ara) %>% 
#   mutate(ratio = if_else(correlation >= 1.25, paste(">=1.25"), paste("1")))

not_correlated <- combined_data %>% 
  mutate(correlation = mean_bs_TF_promoter_Brapa/mean_bs_TF_promoter_Ara) %>% 
  mutate(ratio = if_else(correlation >= 1.25, paste(">=1.25"), paste("1"))) %>% 
  filter(ratio != "1")
  
  
p4 <- combined_data %>% 
  mutate(correlation = mean_bs_TF_promoter_Brapa/mean_bs_TF_promoter_Ara) %>% 
  mutate(ratio = if_else(correlation >= 1.25, paste(">=1.25"), paste("1"))) %>% 
  ggplot(mapping = aes(log(mean_bs_TF_promoter_Brapa), log(mean_bs_TF_promoter_Ara), color = ratio)) +
  geom_point(alpha = 0.4, size = 0.6) +
  coord_cartesian(xlim=c(0,3), ylim=c(0,3)) +
  labs(title = "Mean number of TF binding sites per promoter",
       y = "Arabidopsis_log2(n)",
       x = "Brapa_log2(n)")  +
  theme(plot.title = element_text(size=12)) +
  geom_smooth(method=lm, se=FALSE, fullrange = TRUE, size=0.1) # Remove confidence intervals
                                    # Extend the regression lines
p4


```


####-- save plots
```{r eval=FALSE, include=FALSE}
# basic plot
ggsave(here("plots/rc", "mean_N_bs_TF_promoters_BrapaVsArab_rc_v1.jpeg"), p1, width = 10, height = 10, units = "cm")
# add regression line
ggsave(here("plots/rc", "mean_N_bs_TF_promoters_BrapaVsArab_rc_v2.jpeg"), p2, width = 10, height = 10, units = "cm")
# add statistics
ggsave(here("plots/rc", "mean_N_bs_TF_promoters_BrapaVsArab_rc_v3.jpeg"), p3, width = 10, height = 10, units = "cm")
# plot not corelated TFs
ggsave(here("plots/rc", "mean_N_bs_TF_promoters_BrapaVsArab_rc_v4.jpeg"), p4, width = 10, height = 10, units = "cm")

#rm(p1, p2, p3, p4, Brapa_data_plot, Arab_data_plot, combined_data, res, model)
```





#########--------########

## 3.2. Plot "Mean number of TF binding sites among promoters they bound" in Brapa vs Arabidopsis for *different paralogs*
## look on each homeolog separately, compare total A.thaliana vs B.rapa for each homolog group
####-- Preprocess data, add genome information to Arabidopsis
```{r}
Ara_data_genomes <- Brapa3_TF_results_orthologs_rc %>% filter(motif != "0") %>% 
  select(AGI, genome) %>% 
  unique()  %>%
  inner_join(Ara_TF_results_orthologs_rc, ., by = c("sequence" = "AGI"))



Brapa_data <- Brapa3_TF_results_orthologs_rc %>% filter(motif != "0") %>% 
  select(motif, genome, sequence)

Ara_data <-  Ara_data_genomes %>% 
  select(motif, sequence, genome)


Brapa_data 
Ara_data 

length(unique(Brapa_data$motif)) == length(unique(Ara_data$motif))

```
####-- Preprocess data and plot
```{r}
Brapa_data_plot <- Brapa_data %>% 
  filter(motif != 0) %>%
  group_by(motif, genome, sequence) %>% 
  summarise("N_bs_TF_promoter" = n()) %>% 
  group_by(motif, genome) %>% 
  summarise("mean_bs_TF_promoter" = mean(N_bs_TF_promoter)) 

Ara_data_plot <- Ara_data %>% 
  filter(motif != 0) %>%
  group_by(motif, genome, sequence) %>% 
  summarise("N_bs_TF_promoter" = n()) %>% 
  group_by(motif, genome) %>% 
  summarise("mean_bs_TF_promoter" = mean(N_bs_TF_promoter)) 

combined_data <- 
  full_join(Brapa_data_plot, Ara_data_plot, by = c("motif", "genome"), suffix = c(".Brapa", ".Ara")) %>% 
  mutate_all(., list(~replace(., is.na(.), 0))) 
  
table(is.na(combined_data))
################

################
p1 <- ggplot(combined_data, mapping = aes(mean_bs_TF_promoter.Brapa, mean_bs_TF_promoter.Ara, colour = genome)) +
  geom_point(alpha = 0.4, size = 0.6) +
  ylim(0,max(Brapa_data_plot$mean_bs_TF_promoter, Brapa_data_plot$mean_bs_TF_promoter)) +
  xlim(0,max(Brapa_data_plot$mean_bs_TF_promoter, Brapa_data_plot$mean_bs_TF_promoter)) +
  labs(title = "Mean number of TF binding sites per promoter",
       y = "Arabidopsis",
       x = "Brapa") +
  geom_smooth(method=lm, se=FALSE, fullrange = TRUE, size = 0.3) + # Remove confidence intervals
  theme(plot.title = element_text(size=12)) 


p2 <- ggplot(combined_data, mapping = aes(mean_bs_TF_promoter.Brapa, mean_bs_TF_promoter.Ara, colour = genome)) +
  geom_point(alpha = 0.4, size = 0.6) +
  facet_wrap(~genome ) +
  ylim(0,max(Brapa_data_plot$mean_bs_TF_promoter, Brapa_data_plot$mean_bs_TF_promoter)) +
  xlim(0,max(Brapa_data_plot$mean_bs_TF_promoter, Brapa_data_plot$mean_bs_TF_promoter)) +
  labs(title = "Mean number of TF binding sites per promoter",
       y = "Arabidopsis",
       x = "Brapa") +
  geom_smooth(method=lm, se=FALSE, fullrange = TRUE, size = 0.1) # Remove confidence intervals  # Extend the regression lines
# Extend the regression lines
p1
p2
``` 
 



## 3.3 Plot "*Relative number* of promoters bound by each TF in Brapa homoeologs" Brapa vs Arabidopsis
## Number of promoters have been normalised by total number of promoters in each homeolog group, in Arab and in Brapa

Calculate number of genes in each genome
```{r}

N_LF <-  Brapa3_TF_results_orthologs_rc %>% filter(motif != "0") %>% 
  filter(genome == "LF") %>% select(sequence) %>% unique() %>% count()
N_MF1 <-  Brapa3_TF_results_orthologs_rc %>% filter(motif != "0") %>% 
  filter(genome == "MF1") %>% select(sequence) %>% unique() %>% count()
N_MF2 <-  Brapa3_TF_results_orthologs_rc %>% filter(motif != "0") %>% 
  filter(genome == "MF2") %>% select(sequence) %>% unique() %>% count()
 
N_LF$n
#9804	
N_MF1
#6815	
N_MF2
#5828	

```

Normalise 
```{r}
Brapa_input <- Brapa_data %>% select(genome, motif, sequence) %>% unique() %>% 
  count(genome, motif) %>% 
  mutate(rel_N_promoters_B = if_else(genome == "LF", n/N_LF$n, if_else(genome == "MF1", n/N_MF1$n, n/N_MF2$n)))

Ara_input <- Ara_data %>% select(genome, motif, sequence) %>% unique() %>% 
  count(genome, motif) %>% 
  mutate(rel_N_promoters_A = if_else(genome == "LF", n/N_LF$n, if_else(genome == "MF1", n/N_MF1$n, n/N_MF2$n)))

input <- full_join(Brapa_input, Ara_input, by = c("motif", "genome")) %>% 
  mutate_all(., list(~replace(.,is.na(.), 0)))


p5 <-  ggplot(input, mapping = aes(rel_N_promoters_B, rel_N_promoters_A, color = genome, alpha = 0.25)) +
  geom_point() +
  # xlim(0,max(input$rel_N_promoters_A)) +
  # ylim(0,max(input$rel_N_promoters_A)) +
  labs(title = "Relative number of promoters with TF binding sites in Brapa homoeologs",
       y = "Arabidopsis",
       x = "Brapa") +
  geom_smooth(method=lm, se=FALSE, fullrange = TRUE) # Remove confidence intervals  # Extend the regression lines
p5


p6 <-  ggplot(input, mapping = aes(rel_N_promoters_B, rel_N_promoters_A, color = genome, alpha = 0.25)) +
  geom_point() +
  # xlim(0,max(input$rel_N_promoters_A)) +
  # ylim(0,max(input$rel_N_promoters_A)) +
  labs(title = "Relative number of promoters with TF binding sites in Brapa homoeologs",
       y = "Arabidopsis",
       x = "Brapa") +
  geom_smooth(method=lm, se=FALSE, fullrange = TRUE) + # Remove confidence intervals  # Extend the regression lines
 facet_wrap(~genome )
p6

```



## 3.4 Plot "*Relative number* of *binding sites* for each TF in Brapa homoeologs" Brapa vs Arabidopsis
## Number of promoters have been normalised by total number of promoters in each homeolog group in Brapa and in Ara

```{r}
Brapa_input <- Brapa_data %>% select(genome, motif, sequence) %>% 
  count(genome, motif) %>% 
  mutate(rel_N_promoters_B = if_else(genome == "LF", n/N_LF$n, if_else(genome == "MF1", n/N_MF1$n, n/N_MF2$n)))

Ara_input <- Ara_data %>% select(genome, motif, sequence) %>% 
  count(genome, motif) %>% 
  mutate(rel_N_promoters_A = if_else(genome == "LF", n/N_LF$n, if_else(genome == "MF1", n/N_MF1$n, n/N_MF2$n)))

input <- full_join(Brapa_input, Ara_input, by = c("motif", "genome")) %>% 
  mutate_all(., list(~replace(.,is.na(.), 0)))


p7 <- ggplot(input, mapping = aes(rel_N_promoters_B, rel_N_promoters_A, color = genome, alpha = 0.5)) +
  geom_point() +
  # xlim(0,max(input$rel_N_promoters_A)) +
  # ylim(0,max(input$rel_N_promoters_A)) +
  labs(title = "Relative number of binding sites for each TF in Brapa homoeologs",
       y = "Arabidopsis",
       x = "Brapa") +
  geom_smooth(method=lm, se=FALSE, fullrange = TRUE) # Remove confidence intervals  # Extend the regression lines

p8 <- ggplot(input, mapping = aes(rel_N_promoters_B, rel_N_promoters_A, color = genome, alpha = 0.5)) +
  geom_point() +
  # xlim(0,max(input$rel_N_promoters_A)) +
  # ylim(0,max(input$rel_N_promoters_A)) +
  labs(title = "Relative number of binding sites for each TF in Brapa homoeologs",
       y = "Arabidopsis",
       x = "Brapa") +
  geom_smooth(method=lm, se=FALSE, fullrange = TRUE) +
 # geom_smooth(method=lm, se=FALSE, fullrange = TRUE) + # Remove confidence intervals  # Extend the regression lines
 facet_wrap(~genome )

p7
p8
```


# 3.5 Plot "*Relative number* of promoters bound by each TF in Brapa homoeologs" Brapa vs Arabidopsis normalised by total
## Number of promoters have been normalised by total number of promoters in each homeolog group in Brapa and *total number of orthologous genes in Arabidopsis*

Calculate number of genes in each genome

```{r}

Brapa_input <- Brapa_data %>% select(genome, motif, sequence) %>% unique() %>% 
  count(genome, motif) %>% 
  mutate(rel_N_promoters_B = if_else(genome == "LF", n/N_LF$n, if_else(genome == "MF1", n/N_MF1$n, n/N_MF2$n)))


Ara_n <- Ara_data %>% filter(motif != "0") %>% 
  select(sequence) %>% unique() %>% count()


Ara_input_n <- Ara_data %>% select(genome, motif, sequence) %>% unique() %>% 
  count(genome, motif) %>% 
  mutate(rel_N_promoters_A =  n/Ara_n$n)

N_LF
#9803
N_MF1
#6815	
N_MF2
#5828	
Ara_n
#14115

input_test <- full_join(Brapa_input, Ara_input_n, by = c("motif", "genome")) %>% 
  mutate_all(., list(~replace(., is.na(.), 0)))

p9 <- ggplot(input_test, mapping = aes(rel_N_promoters_B, rel_N_promoters_A, color = genome)) +
  geom_point() +
  # xlim(0,max(input$rel_N_promoters_A)) +
  # ylim(0,max(input$rel_N_promoters_A)) +
  labs(title = "Relative number of promoters having TF binding sites in Brapa homoeologs",
       y = "Arabidopsis",
       x = "Brapa") +
  geom_smooth(method=lm, se=FALSE, fullrange = TRUE) # Remove confidence intervals  # Extend the regression lines

p10 <- ggplot(input_test, mapping = aes(rel_N_promoters_B, rel_N_promoters_A, color = genome)) +
  geom_point() +
  # xlim(0,max(input$rel_N_promoters_A)) +
  # ylim(0,max(input$rel_N_promoters_A)) +
  labs(title = "Relative number of promoters with TF binding sites in Brapa homoeologs",
       y = "Arabidopsis",
       x = "Brapa") +
 # geom_smooth(method=lm, se=FALSE, fullrange = TRUE) + # Remove confidence intervals  # Extend the regression lines
 facet_wrap(~genome )
p9
p10
```



####-- save plots
```{r eval=FALSE, include=FALSE}

ggsave(here("plots/rc/", "Relatve_N_promoters_bound_TF_BrapaVsArab_homologes_rc_v1.jpg"), p5, width = 10, height = 10, units = "cm")
ggsave(here("plots/rc/", "Relatve_N_promoters_bound_TF_BrapaVsArab_homologes_rc_v2.jpg"), p6, width = 10, height = 10, units = "cm")

ggsave(here("plots/rc/", "Relatve_N_bs_TF_promoters_BrapaVsArab_homologes_rc_v1.jpg"), p7, width = 10, height = 10, units = "cm")
ggsave(here("plots/rc/", "Relatve_N_bs_TF_promoters_BrapaVsArab_homologes_rc_v2.jpg"), p8, width = 10, height = 10, units = "cm")
  
ggsave(here("plots/rc/", "Relatve_N_promoters_bound_TF_BrapaVsArab_homologes_rc_v3.jpg"), p9, width = 10, height = 10, units = "cm")
ggsave(here("plots/rc/", "Relatve_N_promoters_bound_TF_BrapaVsArab_homologes_rc_v4.jpg"), p10, width = 10, height = 10, units = "cm")
    
#rm(p5, p6, Brapa_data_plot, Arab_data_plot, combined_data)

```


############
############
#############

```{r}
Brapa3_TF_results_3genomes_rc <- read.csv("~/Work/2019/Analysis/Evoloution_TF_binding_sites_Brassica/output_data/Brapa3_TF_results_3genomes_rc.csv")
 Ara_TF_results_rc <- read.csv("~/Work/2019/Analysis/Evoloution_TF_binding_sites_Brassica/output_data/Ara_TF_results_rc.csv")
 
```



Calculate number of genes in each genome
```{r}
Brapa_data_full <- Brapa3_TF_results_3genomes_rc
Ara_data_full <- Ara_TF_results_rc

N_LF_full <-  Brapa3_TF_results_3genomes_rc %>% filter(motif != "0") %>% 
  filter(genome == "LF") %>% select(sequence) %>% unique() %>% count()
N_MF1_full <-  Brapa3_TF_results_3genomes_rc %>% filter(motif != "0") %>% 
  filter(genome == "MF1") %>% select(sequence) %>% unique() %>% count()
N_MF2_full <-  Brapa3_TF_results_3genomes_rc %>% filter(motif != "0") %>% 
  filter(genome == "MF2") %>% select(sequence) %>% unique() %>% count()
 
N_LF_full$n
	
N_MF1_full$n

N_MF2_full$n

# [1] 13837
# [1] 9751
# [1] 8238

Ara_full_n <- Ara_data_full %>% filter(motif != "0") %>% 
  select(sequence) %>% unique() %>% count()
Ara_full_n$n

# [1] 21821

```

Normalise 
```{r}

Brapa_input_full <- Brapa_data_full %>% select(genome, motif, sequence) %>% unique() %>% 
  count(genome, motif) %>% 
  mutate(rel_N_promoters_B = if_else(genome == "LF", n/N_LF_full$n, if_else(genome == "MF1", n/N_MF1_full$n, n/N_MF2_full$n)))


Ara_input_n <- Ara_data_full %>% select(motif, sequence) %>% unique() %>% 
  count(motif) %>% 
  mutate(rel_N_promoters_A =  n/Ara_full_n$n)



input_full <- full_join(Brapa_input_full, Ara_input_n, by = c("motif")) %>% 
  mutate_all(., list(~replace(., is.na(.), 0)))

p11 <- ggplot(input_full, mapping = aes(rel_N_promoters_B, rel_N_promoters_A, color = genome)) +
  geom_point() +
  # xlim(0,max(input$rel_N_promoters_A)) +
  # ylim(0,max(input$rel_N_promoters_A)) +
  labs(title = "Relative number of promoters having TF binding sites in Brapa homoeologs",
       y = "Arabidopsis",
       x = "Brapa") +
  geom_smooth(method=lm, se=FALSE, fullrange = TRUE) # Remove confidence intervals  # Extend the regression lines

p12 <- ggplot(input_full, mapping = aes(rel_N_promoters_B, rel_N_promoters_A, color = genome)) +
  geom_point() +
  # xlim(0,max(input$rel_N_promoters_A)) +
  # ylim(0,max(input$rel_N_promoters_A)) +
  labs(title = "Relative number of promoters with TF binding sites in Brapa homoeologs",
       y = "Arabidopsis",
       x = "Brapa") +
 # geom_smooth(method=lm, se=FALSE, fullrange = TRUE) + # Remove confidence intervals  # Extend the regression lines
 facet_wrap(~genome )

p11
p12

```



```{r}

Brapa_input <- Brapa_data %>% select(genome, motif, sequence) %>% unique() %>% 
  count(genome, motif) %>% 
  mutate(rel_N_promoters_B = if_else(genome == "LF", n/N_LF$n, if_else(genome == "MF1", n/N_MF1$n, n/N_MF2$n)))


Ara_n <- Ara_data %>% filter(motif != "0") %>% 
  select(sequence) %>% unique() %>% count()


Ara_input_n <- Ara_data %>% select(genome, motif, sequence) %>% unique() %>% 
  count(genome, motif) %>% 
  mutate(rel_N_promoters_A =  n/Ara_n$n)

N_LF
#9803
N_MF1
#6815	
N_MF2
#5828	
Ara_n
#14115

input_test <- full_join(Brapa_input, Ara_input_n, by = c("motif", "genome")) %>% 
  mutate_all(., list(~replace(., is.na(.), 0)))

p9 <- ggplot(input_test, mapping = aes(rel_N_promoters_B, rel_N_promoters_A, color = genome)) +
  geom_point() +
  # xlim(0,max(input$rel_N_promoters_A)) +
  # ylim(0,max(input$rel_N_promoters_A)) +
  labs(title = "Relative number of promoters having TF binding sites in Brapa homoeologs",
       y = "Arabidopsis",
       x = "Brapa") +
  geom_smooth(method=lm, se=FALSE, fullrange = TRUE) # Remove confidence intervals  # Extend the regression lines

p10 <- ggplot(input_test, mapping = aes(rel_N_promoters_B, rel_N_promoters_A, color = genome)) +
  geom_point() +
  # xlim(0,max(input$rel_N_promoters_A)) +
  # ylim(0,max(input$rel_N_promoters_A)) +
  labs(title = "Relative number of promoters with TF binding sites in Brapa homoeologs",
       y = "Arabidopsis",
       x = "Brapa") +
 # geom_smooth(method=lm, se=FALSE, fullrange = TRUE) + # Remove confidence intervals  # Extend the regression lines
 facet_wrap(~genome )
p9
p10
```




















######################################   ######################################  ######################################
######################################   ######################################  ######################################
######################################  ######################################  ######################################
######################################  ######################################
######################################  ######################################



####-- Investigation of TFs binding sites found only in one of two species
```{r}

#  combined_data %>% filter(motif == "MYBrelated_tnt.AT4G12670_col_a_m1" | motif =="MYBrelated_tnt.TRP1_col_m1")
#  Ara_data %>% filter(motif == "MYBrelated_tnt.AT4G12670_col_a_m1" | motif =="MYBrelated_tnt.TRP1_col_m1")
#  Brapa_data %>% filter(motif == "MYBrelated_tnt.AT4G12670_col_a_m1" | motif =="MYBrelated_tnt.TRP1_col_m1")
# # N of promoters predicted to be bond by "MYBrelated_tnt.AT4G12670_col_a_m1" and "MYBrelated_tnt.TRP1_col_m1" in Brapa is 10 times higher in Brapa than in Arabidopsis
#   full_join(Brapa_data_plot, Ara_data_plot, by = c("motif", "genome"), suffix = c(".Brapa", ".Ara")) %>% 
#     filter_all(any_vars(is.na(.))) 

```





## 4. -------General------#######
## 4.1. Plot "distribution of promoter width" in Brapa vs Arabidopsis
### all annotated promoters with predicted TF binding sites included in calculation
####-- Preprocess data plot
```{r}
Brapa_data <- Brapa3_TF_results %>% 
  select(sequence, promoter_width) %>% 
  unique() 

Ara_data <- Ara_TF_results %>% 
  select(sequence, promoter_width) %>% 
  unique() 

combined_data <- data.frame(
  "dataset" = c(
    rep("Brapa", length(Brapa_data$promoter_width)),
        rep("Arabidopsis", length(Ara_data$promoter_width))),
  "promoter_width" = c(Brapa_data$promoter_width, Ara_data$promoter_width))

ggplot(combined_data,aes(x=promoter_width,fill=dataset))+
  geom_histogram(aes(y=0.5*..density..))+
  facet_wrap(~dataset,nrow=2)

p1 <- ggplot(combined_data,aes(x=promoter_width,fill=dataset))+
  geom_histogram(aes(y=0.5*..density..),
                 alpha=0.5,position='identity', binwidth = 50)+
  scale_y_sqrt()+
  labs(title = "Distribution of promoter length",
       x = "Promoter length") +
  theme(plot.title = element_text(size=12)) 

p1
```

```{r}
  combined_data %>% 
  group_by(dataset, promoter_width) %>% 
  summarise(N = n()) %>% #arrange(N)
  ggplot() +
  geom_point(aes(x = promoter_width, y = N, color = dataset, alpha = 0.1)) +
  ylim(0, 25)+
  theme_bw()


```


####-- save plots
```{r}
ggsave(here("plots", "distribution_promoter_width_BrapaVsArab_v1.jpg"), p1, width = 10, height = 10, units = "cm")
    
rm(p1, Brapa_data, Arab_data, combined_data)
```

##-- Plot N of TF binding sites as function of max.scores or length of TF binding site
```{r}
Brapa_data <- Brapa3_TF_results %>% 
  select(sequence, promoter_width) %>% 
  unique() 

Ara_data <- Ara_TF_results %>% 
  select(sequence, promoter_width) %>% 
  unique() 
```


