---
title: "R Notebook"
output: html_notebook
---

Purpose: 
Are nearby sequences of TF's binding sites different?
Extract 10 bases from each TF's binding site ; 
run Random forest to predict if promoter belongs to Arabidopsis or Brassica; 
Use separate model for each TF;
 
# Requers as input:
# *data.rf*
## dataframe created in 04_flanking_sequence_analysis.Rmd
## *all genes, not orthologs!*

## contains folowing columns:
### [1] "motif"          "sequence"       "start"          "stop"           "promoter_width" "score.pct"      "match"          "x"             
### [9] "species"        "upstream_10"    "upstream_9"     "upstream_8"     "upstream_7"     "upstream_6"     "upstream_5"     "upstream_4"
### [17] "upstream_3"     "upstream_2"     "upstream_1"     "downstream_1"   "downstream_2"   "downstream_3"   "downstream_4"   "downstream_5"
### [25] "downstream_6"   "downstream_7"   "downstream_8"   "downstream_9"   "downstream_10"


## 1. Load the required packages
```{r message=FALSE, include=FALSE}
pkgs <- c('here', 'tidyverse','RColorBrewer','gplots',
          'randomForest', 'broom', 'gridExtra',
          'rtracklayer', 'Biostrings', 'GenomicRanges', 'ggpubr')

lapply(pkgs, library, character.only = TRUE)
rm(pkgs)


library(randomForest)
library(e1071)
library(caret)
library("rfUtilities") # to test model significance
library(AUC)

set.seed(999)

```
 

```{r}
x <- Sys.time()
data.rf <- read_csv(here('output_data', 'data_rf_rc_selected.csv'))
list_TFs <- unique(data.rf$motif)
source(file = here("scripts/rf_function.R"))
Sys.time()-x 

```

```{r}

seed.num <- 999
Response.var <- "species"
n.tree = 500 
file_path <- "output_data/rf/rf_TFs/"
# test.data = data.rf
  
# test.data <- data.rf %>%  
#     select(motif, starts_with("upstream"), starts_with("downstream"),contains(Response.var))
# 
# write_csv(test.data, here('output_data', 'data_rf_rc_selected.csv'))
#   
```






```{r}

x <- Sys.time()
test_output_870to872 <- lapply(list_TFs[870:872], function(x)rf.function(TF_name = x, test.data = data.rf, seed.num = seed.num, n.tree = n.tree, Response.var = Response.var, path = file_path ))

Sys.time()-x  

```
No output! test_output_870to872, ND_tnt.FRS9_colamp_a_m1 was only in one specy

```{r}

x <- Sys.time()
test_output_801to869 <- lapply(list_TFs[801:869], function(x)rf.function(TF_name = x, test.data = data.rf, seed.num = seed.num, n.tree = n.tree, Response.var = Response.var, path = file_path ))

Sys.time()-x  

```


```{r}

x <- Sys.time()
test_output_701to800 <- lapply(list_TFs[701:800], function(x)rf.function(TF_name = x, test.data = data.rf, seed.num = seed.num, n.tree = n.tree, Response.var = Response.var, path = file_path ))

Sys.time()-x  

```


```{r}

x <- Sys.time()
test_output_601to700 <- lapply(list_TFs[601:700], function(x)rf.function(TF_name = x, test.data = data.rf, seed.num = seed.num, n.tree = n.tree, Response.var = Response.var, path = file_path ))

Sys.time()-x  

```



```{r}

x <- Sys.time()
test_output_551to600 <- lapply(list_TFs[551:600], function(x)rf.function(TF_name = x, test.data = data.rf, seed.num = seed.num, n.tree = n.tree, Response.var = Response.var, path = file_path ))

Sys.time()-x  

```


```{r}

x <- Sys.time()
test_output_550 <- lapply(list_TFs[550], function(x)rf.function(TF_name = x, test.data = data.rf, seed.num = seed.num, n.tree = n.tree, Response.var = Response.var, path = file_path ))

Sys.time()-x  

```

```{r}

```

To check and save output:
```{r}

# test_output_870to872_df <- bind_rows(test_output_870to872) %>% arrange(desc(test.dev.Accuracy))
# write_csv(test_output_870to872_df, here("output_data/rf/test_output_870to872_df_summary.csv"))


test_output_801to869_df <- bind_rows(test_output_801to869) %>% arrange(desc(test.dev.Accuracy))
write_csv(test_output_801to869_df, here("output_data/rf/test_output_801to869_df_summary.csv"))

test_output_701to800_df <- bind_rows(test_output_701to800) %>% arrange(desc(test.dev.Accuracy))
write_csv(test_output_701to800_df, here("output_data/rf/test_output_701to800_df_summary.csv"))



test_output_601to700_df <- bind_rows(test_output_601to700) %>% arrange(desc(test.dev.Accuracy)) %>%
write_csv(., here("output_data/rf/test_output_601to700_df_summary.csv"))

test_output_550to600_df <- bind_rows(test_output_550, test_output_551to600) %>% arrange(desc(test.dev.Accuracy)) %>%
write_csv(., here("output_data/rf/test_output_550to600_df_summary.csv"))


```













```{r}
 rf_ABI3VP1_tnt.AT5G18090_col_a_m1_model <-  readRDS("output_data/rf/rf_TFs/ABI3VP1_tnt.AT5G18090_col_a_m1_rf.rda")
```


```{r}
names(rf_ABI3VP1_tnt.AT5G18090_col_a_m1_model)

rf_ABI3VP1_tnt.AT5G18090_col_a_m1_model$test.dev.cm
rf_ABI3VP1_tnt.AT5G18090_col_a_m1_model$test.val.cm$overall["McnemarPValue"]

names(rf_ABI3VP1_tnt.AT5G18090_col_a_m1_model$test.val.cm$overall)


```
[1] "test.rf"     "test.dev.cm" "test.val.cm" "test.dev"    "test.val"    "sample.ind" 


To check output:
```{r}

test_output_df <- bind_rows(test_output, test_output_2, test_output_3, test_output_4)
test_output_df %>% arrange(desc(test.-dev.Accuracy))

write_csv(test_output_df, here("output_data/rf/rf_100_TFs_output_summary.csv"))

```



