# To start run run_to_start.R:

the following folders will be created:
# raw_data
# data contains the raw data files used in the project. These files should not be altered and are ideally read-only.
# 
# docs
# docs contains any manuscripts or interim summaries produced with the project.
# 
# plots
# plots contains any plots, images, tables, or figures created and saved by your code. It should be possible to delete and regenerate this folder with the scripts in the project folder.
# 
# output_data
# output contains non-figure objects created by the scripts. For example, processed data or logs.
# 
# scripts
# scripts is a folder for any files you may want to source() in your scripts. 

# Then run pkgs.R to download and update all packages

The source of the files in raw_data:

scp -r ~/Work/2019/Data/JIC/Brapa/reference/3genomes_AKBr_v3_1810.txt .
scp -r ~/Work/2019/Analysis/Promoters/BRapa/Brapa_v3.0/promoters/Brapa_3.0_promoters_1500.gff3 .
scp -r ~/Work/2019/Analysis/TF_binding/Brapa_TF/Brapa_3/analysed_data/TF_scan_results_Brapa3_promoters1500_TF_family.csv .
scp -r ~/Work/2019/Analysis/Promoters/promoters_1500.gff3 .
scp -r ~/Work/2019/Analysis/TF_binding/analysed_data/TF_scan_results_promoters1500_TF_family_v1.csv .
scp -r ~/Work/2019/Analysis/Arabidopsis/Expression/Clust_phyton/updated_promoter/clust_output_expression/clust_zscoresVariableGenes_triplicate_10TPM_prom_no_op.csv .

TF binding site prediction on both sides:

"~/Work/2019/Analysis/TF_binding/Brapa_TF/Brapa_3/analysed_data/TF_rc_scan_results_Brapa3_promoters1500_TF_family.csv"
"~/Work/2019/Analysis/TF_binding/analysed_data/TF_rc_scan_results_promoters1500_TF_family.csv"

The scripts:
01_preprocess_data_rc.Rmd
# This script reads in raw data from the TF predictions in Brassica and Arabidopsis files and runs preprocessing stream on the data.
# A preprocessed data frame is generated and saved as csv files in output_data/ 


01_1_preprocess_data_rc.Rmd
# This script reads in raw data from the second run of TF predictions on both strands (reverse compliment) in Brassica and Arabidopsis and runs preprocessing stream on the data.
# A preprocessed data frame is generated and saved as "_cr.csv" files in output_data/ 




